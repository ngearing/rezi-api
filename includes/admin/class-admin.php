<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/admin
 */

namespace Rezi\Admin;

use Rezi\Controllers\Properties_Queue;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/admin
 * @author     Your Name <email@example.com>
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The plugin slug name.
	 *
	 * @var string
	 */
	private $plugin_slug;

	/**
	 * The plugin Title
	 *
	 * @var string
	 */
	private $plugin_title;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, \Rezi\Controllers\Api_Controller $api ) {

		$this->load_dependencies();

		$this->plugin_name  = $plugin_name;
		$this->plugin_slug  = str_replace( '-', '_', $plugin_name );
		$this->plugin_title = ucwords( str_replace( '-', ' ', $plugin_name ) );
		$this->version      = $version;

		$this->process_queue = new Properties_Queue();

		$this->api = $api;
		$this->log = new \Katzgrau\KLogger\Logger( REZI_API_DIR . 'logs/' );

	}

	/**
	 * Load required classes
	 *
	 * @return void
	 */
	private function load_dependencies() {
		// require_once REZI_API_DIR . 'includes/modules/class-wp-async-request.php';
		// require_once REZI_API_DIR . 'includes/modules/class-wp-background-process.php';
		// require_once REZI_API_DIR . 'includes/controllers/class-queue-controller.php';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rezi-api-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rezi-api-admin.js', array( 'jquery' ), $this->version, false );
	}

	/**
	 * Adds admin page
	 *
	 * @return void
	 */
	public function add_admin_pages() {

		// Add admin page.
		add_submenu_page(
			'options-general.php', // Parent slug.
			'Rezi Api Options',    // Page title.
			'Rezi Api',            // Menu title.
			'manage_options',      // Capability.
			'rezi-api',            // Menu slug.
			[ $this, 'render_admin_page' ] // Callable function.
		);

	}

	/**
	 * Render the content for the admin page
	 *
	 * @return void
	 */
	public function render_admin_page() {
		// check user capabilities.
		if ( ! current_user_can( 'manage_options' ) ) {
			printf( __( '<p>You are not authorized to perform this operation.</p>', $this->plugin_name ) );
			return;
		}

		$this->print_admin_notice();

		include_once 'partials/rezi-api-admin-display.php';

	}

	/**
	 * Save Api key from form.
	 *
	 * @return void
	 */
	public function admin_save_api_key() {
		// Process Nonce.
		if (
			! isset( $_POST['action'] ) ||
			! $_POST['action'] === 'rezi_save_api_key' ||
			! isset( $_POST['rezi_nonce'] ) ||
			! wp_verify_nonce( $_POST['rezi_nonce'], 'rezi_save_api_key' )
		) {
			printf( __( '<p>Sorry, your nonce did not verify.</p>', $this->plugin_name ) );
			wp_die();
		}

		// Process Form.
		$redirect = esc_url( $_POST['_wp_http_referer'] );
		$api_key  = sanitize_key( $_POST['api_key'] );

		$option_updated = update_option( 'rezi_api_options', [ 'api_key' => $api_key ] );

		// Send Response.
		$this->admin_redirect(
			$redirect,
			[
				'status'  => $option_updated ? 'success' : 'error',
				'message' => $option_updated ? 'Api Key Saved!' : 'Oops, something went wrong.',
			]
		);
	}

	public function admin_import_properties() {
		// Process Nonce.
		if (
			! isset( $_GET['action'] ) ||
			! $_GET['action'] === 'rezi_import_properties' ||
			! isset( $_GET['rezi_nonce'] ) ||
			! wp_verify_nonce( $_GET['rezi_nonce'], 'rezi_import_properties' )
		) {
			printf( __( '<p>Sorry, your nonce did not verify.</p>', $this->plugin_name ) );
			wp_die();
		}

		// Process Form.
		$redirect = esc_url( $_GET['_wp_http_referer'] );

		$check = $this->api->check_api( [ 'body' => [ 'PageSize' => 100 ] ] );

		// Send Response.
		$this->admin_redirect(
			$redirect,
			[
				'status'  => $check ? 'success' : 'error',
				'message' => $check ? 'Success' : 'Oops, something went wrong.',
			]
		);
	}

	/**
	 * Update properties from imported data.
	 *
	 * @return void
	 */
	public function admin_update_properties() {
		// Process Nonce.
		if (
			! isset( $_POST['action'] ) ||
			! $_POST['action'] === 'rezi_update_properties' ||
			! isset( $_POST['rezi_nonce'] ) ||
			! wp_verify_nonce( $_POST['rezi_nonce'], 'rezi_update_properties' )
		) {
			printf( __( '<p>Sorry, your nonce did not verify.</p>', $this->plugin_name ) );
			wp_die();
		}

		// Process Form.
		$redirect = esc_url( $_POST['_wp_http_referer'] );

		$this->api->do_import();

		// Send Response.
		$this->admin_redirect(
			$redirect,
			[
				'status'  => 'success',
				'message' => 'The properties are being updated in the background.',
			]
		);
	}

	public function display_log() {
		$log = $this->log->getLogFilePath();
		$log = new \SplFileObject( $log );

		while ( ! $log->eof() ) {
			echo '<p>' . $log->fgets() . '</p>';
		}

	}

	/**
	 * Redirect to admin page with notices.
	 *
	 * @param string $url The admin page url.
	 * @param array  $notice Notices to display at the page.
	 * @return void
	 */
	private function admin_redirect( $url, $notice = [] ) {
		$redirect = esc_url_raw(
			add_query_arg(
				[ 'rezi_admin_notice' => $notice ],
				$url
			)
		);

		wp_safe_redirect( $redirect );
		// wp_die();
	}

	/**
	 * Print admin notices pass to this page.
	 *
	 * @return void
	 */
	private function print_admin_notice() {
		if ( isset( $_REQUEST['rezi_admin_notice'] ) ) {
			$notice = $_REQUEST['rezi_admin_notice'];

			printf(
				'<div class="notice notice-%s is-dismissible"><p>%s</p></div>',
				esc_html( $notice['status'] ),
				esc_html( $notice['message'] )
			);
		}
	}
}
