<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/admin/partials
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
?>

<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<form id="rezi_save_api_key" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
		<?php wp_nonce_field( 'rezi_save_api_key', 'rezi_nonce' ); ?>
		<input type="hidden" name="action" value="rezi_save_api_key">

		<label for="api_key">Api Key</label>
		<textarea name="api_key" id="api_key" cols="30" rows="10"><?php echo esc_textarea( get_option( 'rezi_api_options' )['api_key'] ); ?></textarea>

		<p class="submit">
			<button type="submit" name="submit" id="submit" class="button button-primary">Save Api Key</button>
		</p>
	</form>

	<div class="row">
		<div class="col">

			<form id="rezi_import_properties" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="get">
				<?php wp_nonce_field( 'rezi_import_properties', 'rezi_nonce' ); ?>
				<input type="hidden" name="action" value="rezi_import_properties">

				<p class="submit">
					<button type="submit" name="submit" id="submit" class="button button-primary btn btn-warning">Re-import All Properties Now</button>
				</p>

				<p class="info">Warning: this may take a while, depending on the number of images to download.</p>

				<div class="response">
					<?php
					$last_request_date = get_transient( 'rezi_last_property_request_date' );
					if ( $last_request_date ) {
						$time = DateTime::createFromFormat( 'Y-m-d\TH:i:s\Z', $last_request_date );
						$time->setTimezone( new DateTimeZone( 'Australia/Victoria' ) );
						printf(
							'<p><strong>Api Last Checked:</strong> %s</p>
							<p><strong>Properties Found Last Check:</strong> %d properties.</p>
							<p><strong>Next Api Check: </strong> %s </p>',
							$time->format( 'Y-m-d H:i:s' ),
							get_transient( 'rezi_total_api_posts' ),
							wp_next_scheduled( 'rezi_check_api' ) ? get_date_from_gmt( date( 'Y-m-d H:i:s', wp_next_scheduled( 'rezi_check_api' ) ) ) : 'Not scheduled'
						);
					}
					?>
				</div>

				<div class="response">
					<?php
					$next_p_import = wp_next_scheduled( 'rezi_import_properties' );
					$next_a_import = wp_next_scheduled( 'rezi_import_attachments' );
					printf(
						'<p><strong>Next Scheduled Property Import:</strong> %s</p>
						<p><strong>Next Scheduled Media Import:</strong> %s</p>',
						$next_p_import ? get_date_from_gmt( date( 'Y-m-d H:i:s', $next_p_import ) ) : 'not scheduled',
						$next_a_import ? get_date_from_gmt( date( 'Y-m-d H:i:s', $next_a_import ) ) : 'not scheduled'
					);
					?>
				</div>
			</form>

		</div><!-- .col -->

		<div class="col" style="background:white;padding:1em;margin:2em auto;">
			<h4>Log</h4>
			<pre style="max-height: 650px; overflow: auto;">
				<?php $this->display_log(); ?>
			</pre>
		</div>

	</div><!-- .row -->

</div><!-- .wrap -->
