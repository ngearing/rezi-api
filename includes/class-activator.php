<?php
/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 */

namespace Rezi;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 * @author     Your Name <email@example.com>
 */
class Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// Cron Jobs.
		if ( ! wp_next_scheduled( 'rezi_check_api' ) ) {
			wp_schedule_event( time(), 'hourly', 'rezi_check_api' );
		}

		if ( ! wp_next_scheduled( 'rezi_import_properties' ) ) {
			wp_schedule_event( time(), 'hourly', 'rezi_import_properties' );
		}

		if ( ! wp_next_scheduled( 'rezi_import_attachments' ) ) {
			wp_schedule_event( time(), 'hourly', 'rezi_import_attachments' );
		}
	}

}
