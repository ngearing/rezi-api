<?php
/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 */

namespace Rezi;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 * @author     Your Name <email@example.com>
 */
class Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook( 'rezi_check_api' );
		wp_clear_scheduled_hook( 'rezi_import_properties' );
		wp_clear_scheduled_hook( 'rezi_import_attachments' );
	}

}
