<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 */

namespace Rezi;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rezi_Api
 * @subpackage Rezi_Api/includes
 * @author     Your Name <email@example.com>
 */
class Rezi_Api {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rezi_Api_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'REZI_API_VERSION' ) ) {
			$this->version = REZI_API_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rezi-api';

		$this->load_dependencies();
		$this->define_cron_hooks();
		$this->define_admin_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rezi_Api_Loader. Orchestrates the hooks of the plugin.
	 * - Rezi_Api_i18n. Defines internationalization functionality.
	 * - Rezi_Api_Admin. Defines all hooks for the admin area.
	 * - Rezi_Api_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		$this->loader = new \Rezi\Modules\Loader();

		/**
		 * The class responsible for defining all actions related to the api.
		 */
		$this->api = new \Rezi\Controllers\Api_Controller( $this->get_plugin_name(), $this->get_version() );

		$this->property_queue   = new \Rezi\Controllers\Properties_Queue();
		$this->attachment_queue = new \Rezi\Controllers\Attachment_Queue();
	}

	/**
	 * Register all hooks related to api functionality.
	 *
	 * @return void
	 */
	private function define_cron_hooks() {

		// Crons.
		$this->loader->add_action( 'rezi_check_api', $this->api, 'check_api' );

		$this->loader->add_action( 'rezi_import_properties', $this->property_queue, 'maybe_handle' );

		$this->loader->add_action( 'rezi_import_attachments', $this->attachment_queue, 'maybe_handle' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new \Rezi\Admin\Admin( $this->get_plugin_name(), $this->get_version(), $this->api );

		// Styles & Scripts.
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Register admin menu page.
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_admin_pages' );

		// When a form is submitted to admin-post.php.
		$this->loader->add_action( 'admin_post_rezi_save_api_key', $plugin_admin, 'admin_save_api_key' );
		$this->loader->add_action( 'admin_post_rezi_import_properties', $plugin_admin, 'admin_import_properties' );
		$this->loader->add_action( 'admin_post_rezi_update_properties', $plugin_admin, 'admin_update_properties' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rezi_Api_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
