<?php
/**
 * Api Controller file.
 *
 * @package Rezi_Api
 */

namespace Rezi\Controllers;

use Rezi\Modules\Api;
use Rezi\Controllers\Properties_Queue;

/**
 * Api_Controller handles information to and from the api.
 */
class Api_Controller {

	/**
	 * Construct function, handles init loading and variable setting.
	 */
	public function __construct() {
		$this->load_dependencies();
	}

	/**
	 * Load the dependencies required by this controller class.
	 *
	 * @return void
	 */
	private function load_dependencies() {
		/**
		 * The api module class which handles getting and returning data.
		 */
		$this->api = new Api();

		/**
		 * Queue Controller handles the queue.
		 */
		$this->process_queue = new Properties_Queue();

		$this->log = new \Katzgrau\KLogger\Logger( REZI_API_DIR . 'logs/' );
	}

	/**
	 * Check the api for new properties
	 *
	 * @param array $request_data The request data to pass to the api.
	 * @return bool
	 */
	public function check_api( $request_data = [] ) {

		if ( defined( 'DOING_CRON' ) || ! $request_data ) {
			$request_data = [
				'body' => [
					'LastUpdated' => get_transient( 'rezi_last_property_request_date' ),
				],
			];
			$this->api->set_request_data( $request_data );
		}

		$request = $this->api->request();

		if ( ! is_wp_error( $request ) && 200 === $request['response']['code'] ) {
			$body         = json_decode( $request['body'] );
			$count        = $body->TotalCount;
			$posts        = $body->Collection;
			$request_date = gmdate( 'Y-m-d\TH:i:s\Z' );

			$this->log->info( 'Successful Api Request' );
			$this->log->debug( 'Request', $this->api->get_request_data() );
			$this->log->debug( "Response found $count items" );

			$this->last_request = $body;
			set_transient( 'rezi_last_property_request', $body );
			set_transient( 'rezi_last_property_request_date', $request_date );
			set_transient( 'rezi_total_api_posts', $count );

			if ( $posts ) {
				foreach ( $posts as $item ) :
					// Add items to background processor.
					$this->process_queue->push_to_queue( $item );
				endforeach;

				// Start processing.
				$this->process_queue->save()->dispatch();
				$this->log->info( 'Item import queue dispatched' );

				file_put_contents( REZI_API_DIR . 'import.json', json_encode( $body, JSON_PRETTY_PRINT ) );
			}

			return true;
		}

		return false;
	}

	/**
	 * Delete posts that are no longer in the api.
	 *
	 * @return void
	 */
	private function delete_posts() {
		$last_request = $this->last_request ?: get_transient( 'rezi_last_property_request' );

		$property_ids = [];
		$property_ids = array_map(
			function( $item ) {
				return (int) $item->RoleId;
			},
			$last_request->Collection
		);

		global $wpdb;
		$posts = $wpdb->get_col(
			"SELECT post_id
				FROM $wpdb->postmeta
				WHERE meta_key = 'estate_property_id'
				AND meta_value NOT IN ( " . implode( ',', $property_ids ) . ' ) '
		);

		if ( ! $posts ) {
			return;
		}

		foreach ( $posts as $post ) {
			wp_delete_post( (int) $post );
		}

	}

	/**
	 * Schedule imports.
	 */
	public function do_import() {
		$import = get_transient( 'rezi_import_backlog' );

		// Start the import process.
		foreach ( $import as $item ) :
			// Add items to background processor.
			$this->process_queue->push_to_queue( $item );
		endforeach;

		// Start processing.
		$this->process_queue->save()->dispatch();
	}
}
