<?php

namespace Rezi\Controllers;

use DateTime;
use DateTimeZone;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Rezi\Modules\Property;
use Rezi\Modules\Attachment;
use Rezi\Modules\ArrayUtils;

class Properties_Controller {

	public static function run_import() {
		$path        = REZI_API_DIR . 'import.json';
		$import_data = file_get_contents( $path );
		$import_data = json_decode( $import_data );

		if ( ! $import_data ) {
			return;
		}

		foreach ( $import_data->Collection as $item ) {
			self::import_property( $item );
		}
	}

	public static function import_property( $data ) {
		$property = Property::get_by_import( $data );
		// Map import data to post fields.
		$post_data = ( new Properties_Controller() )->get_property_data( $data );

		if ( ! $property ) {
			// Create post from data.
			$property = Property::create( $post_data );
		}

		// Set post_id for post updates.
		$post_data['ID'] = $property->post_id;

		// Check if import data has been updated.
		if ( $property->get_import_data() ) {
			$import_updated = $data->LastUpdated;
			$property_updated = $property->get_import_data()->LastUpdated;

			// Update post with data.
			$property->update( $post_data );
		}

		// Hack Fix post date not being imported.
		$dates = [
			'post_date',
			'post_date_gmt',
			'post_modified',
			'post_modified_gmt',
		];
		foreach ($dates as $date ) {
			if ( ! isset( $post_data[ $date ] ) || false == $post_data[ $date ] ) {
				$post_data[ $date ] = $property->post_data->$date;
			}
		}

		// Import meta and terms using old class.
		// TODO: move functionality out of this class and delete it.
		$import = new \Rezi\Modules\Import_Post( $post_data );

		// Check data for images / documents to download.
		if ( isset( $post_data['post_attachments'] ) ) :

			$attachment_queue = isset( $attachment_queue ) ? $attachment_queue : new Attachment_Queue();

			foreach ( $post_data['post_attachments'] as $meta_key => $data ) :

				// $data can be string or array.
				if ( ! is_array( $data ) ) {
					// create or get attachment.
					$attachment = ( new Properties_Controller() )->get_attachment( $data );
					$property->set_meta( [ $meta_key => $attachment->post_id ] );

					// schedule for file download.
					if ( ! file_exists( $attachment->get_file() ) ) {
						$attachment_queue->push_to_queue(
							[
								'parent'  => $property->post_id,
								'post_id' => $attachment->post_id,
								'url'     => $data,
							]
						);
					}
				} else {
					array_walk_recursive(
						$data,
						function( &$item, $key ) use ( $attachment_queue, $property ) {
							// create or get attachment.
							$attachment = ( new Properties_Controller() )->get_attachment( $item );

							// schedule for file download.
							if ( ! file_exists( $attachment->get_file() ) ) {
								$attachment_queue->push_to_queue(
									[
										'parent'  => $property->post_id,
										'post_id' => $attachment->post_id,
										'url'     => $item,
									]
								);
							}

							$item = $attachment->post_id;
						}
					);

					$property->set_meta( [ $meta_key => $data ] );
				}
			endforeach;

			if ( $attachment_queue ) {
				$attachment_queue->save()->dispatch();
			}

		endif;
	}

	public static function get_attachment( $url ) {
		$attachment = Attachment::get_by_file( $url );

		if ( ! $attachment ) {
			// Create attachment with no file to get id.
			$file     = pathinfo( $url );
			$filename = sprintf(
				'%s.%s',
				$file['filename'],
				strpos( $file['extension'], '?' ) !== false ?
					explode( '?', $file['extension'] )[0] :
					$file['extension']
			);

			$post_id    = wp_insert_attachment(
				[
					'file'       => $filename,
					'post_title' => $file['filename'],
				]
			);
			$attachment = new Attachment( $post_id );
		}

		return $attachment;
	}

	public static function import_attachment( $item ) {

		$url        = esc_url( wp_unslash( $item['url'] ) );
		$url_parsed = parse_url( $url );
		$url_info   = pathinfo( $url_parsed['path'] );
		$file       = $url_info['basename'];
		$url_query  = [];
		parse_str( $url_parsed['query'], $url_query );

		$parent = $item['parent'];

		$attachment = new \Rezi\Modules\Attachment( $item['post_id'] );
		$exists     = file_exists( $attachment->get_file() );
		$same       = $file == pathinfo( $attachment->get_file() )['basename'];

		if ( ! $attachment ) {
			error_log(
				json_encode(
					[
						'import_attachment' => 'attachment not found',
						$item,
					]
				)
			);
			return false;
		}

		// If attachment has file and is same as one we are trying to import, return.
		if ( $exists && $same ) {
			error_log(
				json_encode(
					[
						'import_attachment' => 'file already exists.',
						$item,
					]
				)
			);
			return false;
		}

		$property_control = new Properties_Controller();

		// Check if file exists in uploads, not linked to a post.
		$already_downloaded = $property_control->file_downloaded( $file );
		if ( $already_downloaded ) {
			return $property_control->update_attachment_file( $already_downloaded, $attachment );
		}

		// If file is an image.
		if ( in_array( $url_info['extension'], [ 'jpg', 'png' ] ) ) {
			// Add params to get smaller image.
			$url_query['width']   = 2000;
			$url_query['quality'] = 40;

			$url = str_replace( $url_parsed['query'], http_build_query( $url_query ), $url );
		}

		$downloaded_file = $property_control->download_file( $url );
		if ( $downloaded_file && ! is_wp_error( $downloaded_file ) ) {
			$attachment->set_meta( [ 'original_src' => $url ] );

			return $property_control->update_attachment_file( $downloaded_file, $attachment );
		}
	}


	public function get_property_data( $item ) {
		// Map out the import data to where we want it to go.
		$features = $this->getObjectByKeyVal( [ 'Name' => 'Features' ], $item->Descriptions );
		// This needs to be array of arrays to support array_column in php 5.xxx.
		$features = $features ? json_decode( json_encode( $features[0]->Features ), true ) : [];
		// Sort features by Order value.
		$features = array_column( $features, 'Feature', 'Order' );
		ksort( $features );

		$room_count = $item->RoomCountsDescription;
		$address    = $item->Address;
		$title      = $this->findAdjacentByKeyValuePair( $item->Descriptions, 'Name', 'Property Name - NOT FOR PORTALS', 'Text' ) ?: $item->RoleId;
		
		$documents  = $item->Documents;
		foreach( $documents as $doc_obj ) {
			$documents[ $doc_obj->DocumentSubType->SystemName ][] = $doc_obj->Url;
		}

		$images = $item->Images;
		$feat_image = $item->Images[0][ 'Url' ];
		$f_key = array_search(true, array_column( $images, 'IsPrimaryImage' ) );
		if ( $f_key ) {
			$feat_image = $images[ $f_key ][ 'Url' ];
			unset( $images[ $f_key ] );
		}

		// Locations meta.
		$locations = strip_tags( $this->findAdjacentByKeyValuePair( $item->Descriptions, 'Name', 'Local Area', 'Text' ) );
		$locations = explode( ',', $locations );
		$locations = array_map('trim', $locations);

		$import_data = [
			'post_author'       => 1,
			'post_date'         => $this->changeDateZone( 'Y-m-d\TH:i:s.u\Z', $item->DateInstructed, 'Y-m-d H:i:s', date_default_timezone_get() ),
			'post_date_gmt'     => $this->changeDateZone( 'Y-m-d\TH:i:s.u\Z', $item->DateInstructed, 'Y-m-d H:i:s', 'GMT' ),
			'post_content'      => $this->findAdjacentByKeyValuePair( $item->Descriptions, 'Name', 'Main Marketing', 'Text' ),
			'post_title'        => strip_tags( $title ),
			'post_excerpt'      => $this->findAdjacentByKeyValuePair( $item->Descriptions, 'Name', 'SummaryText', 'Text' ),
			'post_modified'     => $this->changeDateZone( 'Y-m-d\TH:i:s.u\Z', $item->LastUpdated, 'Y-m-d H:i:s', date_default_timezone_get() ),
			'post_modified_gmt' => $this->changeDateZone( 'Y-m-d\TH:i:s.u\Z', $item->LastUpdated, 'Y-m-d H:i:s', 'GMT' ),
			'post_parent'       => 0,
			'post_type'         => 'property',
			'post_meta'         => [
				'estate_local_area_names'        => $locations[0],
				'estate_property_google_maps'    => [
					'address' => sprintf(
						'%s %s, %s %s, UK',
						$address->Number,
						$address->Street,
						$address->Town,
						$address->Postcode
					),
					'lat'     => $address->Location->Latitude,
					'lng'     => $address->Location->Longitude,
				],
				'estate_property_id'             => $item->RoleId,
				'estate_property_featured'       => in_array( 'Featured', array_column( $item->Flags, 'DisplayName' ) ),
				'estate_property_available_from' => $this->changeDateZone( 'Y-m-d\TH:i:s\Z', ( isset( $item->AvailableDate ) ? $item->AvailableDate : false ), 'Y-m-d H:i:s', date_default_timezone_get() ),
				'estate_property_price'          => $item->Price->PriceValue,
				'estate_property_price_suffix'   => $item->RoleType->DisplayName === 'Letting' ?
					( isset( $item->Price->PriceType->DisplayName ) ? str_replace( 'Weekly', 'Per Week', $item->Price->PriceType->DisplayName ) : '' ) :
					( null !== $this->recursiveFind( $item->Descriptions, 'LeaseType' ) ? $this->recursiveFind( $item->Descriptions, 'LeaseType' )[0]->DisplayName : '' ),
				'estate_property_size_meters'    => $this->arrayPregMatch( '/(\d+\.?\d+)(?:\s+?[sq]\D*?)?m/mi', $features ),
				'estate_property_size_feet'      => $this->arrayPregMatch( '/(\d+)\W+?(?:sq\D*?\bft|sqft)/mi', $features ),
				'estate_property_receptions'     => $room_count->Receptions,
				'estate_property_bedrooms'       => $room_count->Bedrooms,
				'estate_property_bathrooms'      => $room_count->Bathrooms,
				'estate_property_garages'        => isset( $item->AmenityDescription->Garages ) ? $item->AmenityDescription->Garages : 0,
				'estate_property_attachments_repeater' => [],
				'estate_property_floor_plans'          => [],
				'estate_property_epc'                  => [],
			],
			'post_attachments'  => [
				'_thumbnail_id'                        => esc_url( $feat_image ),
				'estate_property_gallery'              => array_column( $item->Images, 'Url' ),
			],
		];


		// Set Status update.
		$statuses = [
			'Selling' => [
				'On Market'      => 'For Sale',
				'Under Offer'    => 'Sale Agreed',
				'Offer Accepted' => 'Sold',
			],
			'Letting' => [
				'On Market'      => 'To Let',
				'Under Offer'    => 'Let Agreed',
				'Offer Accepted' => 'Let',
			],
		];
		$status   = $statuses[ $item->RoleType->DisplayName ];
		$flags    = array_column(
			array_map(
				function( $f ) {
					return (array) $f;
				},
				$item->Flags
			),
			'DisplayName'
		);
		$status   = array_filter(
			$status,
			function( $v, $k ) use ( $flags ) {
				return in_array( $k, $flags );
			},
			ARRAY_FILTER_USE_BOTH
		);
		$import_data['post_meta']['estate_property_status_update'] = array_pop( $status );

		// Property Attachments / Brochures.
		if ( isset( $documents['Brochure']) ) {
			foreach($documents['Brochure'] as $doc ) {
				$import_data['post_meta']['estate_property_attachments_repeater'][] = [
					'file' => $doc,
				];
			}
		}

		// Property Floor Plans.
		if ( isset( $documents['Floorplan'] )) {
			foreach( $documents['Floorplan'] as $doc ) {
				$import_data['post_meta']['estate_property_floor_plans'][] = [
					'file' => $doc,
				];
			}
		}

		// Property EPC.
		if ( isset( $item->EPC ) ) {
			$import_data['post_meta']['estate_property_epc'][] = [
				'file' => $item->EPC->Image->Url,
			];
		} else if ( isset( $documents['EPC'] )) {
			foreach( $documents['EPC'] as $doc ) {
				$import_data['post_meta']['estate_property_epc'][] = [
					'file' => $doc,
				];
			}
		}

		// Property Video. Virtual Tour
		if (isset( $documents['VirtualTour'] )) {
			foreach ($documents['VirtualTour'] as $doc) {
				$import_data['post_meta']['estate_property_video_id'] = $doc;
			}
		}

		$post_terms                = [
			'property-location' => $locations,
			'property-type'     => strip_tags( $item->PropertyType->DisplayName ),
			'property-status'   => str_replace( [ 'Letting', 'Selling' ], [ 'To Let', 'For Sale' ], strip_tags( $item->RoleType->DisplayName ) ),
			'property-features' => $features,
		];
		$import_data['post_terms'] = $post_terms;

		$import_data['post_meta']['import_data'] = json_encode( $item );

		return $import_data;
	}

	public function changeDateZone( $in_format = '', $in_date, $return_format, $timezone = 'Australia/Victoria' ) {
		if ( ! $in_date ) {
			return;
		}
		$return_date = new DateTime($in_date);
		if ( ! $return_date ) {
			$return_date = DateTime::createFromFormat( $in_format, $in_date );
		}
		if ( ! $return_date ) {
			return false;
		}
		$return_date->setTimezone( new DateTimeZone( $timezone ) );

		return $return_date->format( $return_format );
	}

	public function arrayPregMatch( $pattern, $array ) {
		foreach ( $array as $value ) {

			preg_match( $pattern, $value, $matches );
			if ( $matches ) {
				return $matches[1];
			}
		}

		return false;
	}

	public function getObjectByKeyVal( $search = [], $array ) {
		$results = [];

		foreach ( $search as $search_key => $search_value ) {
			foreach ( $array as $arr_key => $arr_val ) {
				if ( isset( $arr_val->$search_key ) && $search_value == $arr_val->$search_key ) {
					$results[] = $array[ $arr_key ];
				}
			}
		}

		return $results;
	}

	public function findAdjacentByKeyValuePair( $array = [], $search_key = '', $search_value = '', $value_key = '' ) {
		foreach ( $array as $item ) {

			if ( is_array( $item ) && isset( $item[ $search_key ] ) && $search_value === $item[ $search_key ] ) {
				if ( isset( $item[ $value_key ] ) ) {
					return $item[ $value_key ];
				}
			} elseif ( is_object( $item ) && isset( $item->$search_key ) && $search_value === $item->$search_key ) {
				if ( isset( $item->$value_key ) ) {
					return $item->$value_key;
				}
			}
		}

		return false;
	}

	public function recursiveFind( array $array, $needle ) {
		$iterator  = new RecursiveArrayIterator( $array );
		$recursive = new RecursiveIteratorIterator( $iterator, RecursiveIteratorIterator::SELF_FIRST );
		$aHitList  = array();
		foreach ( $recursive as $key => $value ) {
			if ( $key === $needle ) {
				array_push( $aHitList, $value );
			}
		}
		return $aHitList;
	}

	public function update_attachment_file( $file, $attachment ) {

		$file       = new \SplFileInfo( $file );
		$attachment = is_a( $attachment, 'Rezi\Modules\Attachment', true ) ? $attachment : new Attachment( $attachment );

		$post_data = array_merge(
			(array) $attachment->post,
			[
				'ID'             => $attachment->post_id,
				'post_mime_type' => mime_content_type( $file->getRealPath() ),
				'post_content'   => '',
			]
		);

		$filename = strpos( $file->getRealPath(), 'uploads/' ) !== false ? array_pop( explode( 'uploads/', $file->getRealPath() ) ) : $file->getFilename();

		$updated = wp_insert_attachment( $post_data, $filename, $attachment->post->post_parent, true );
		if ( is_wp_error( $updated ) ) {
			error_log(
				json_encode(
					[
						'import_attachment' => 'post update error',
						$updated,
					]
				)
			);
			return $updated;
		}

		// If run by cron wp-admin is not loaded.
		if ( defined( 'DOING_CRON' ) || ! function_exists( 'wp_generate_attachment_metadata' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';
			require_once ABSPATH . 'wp-admin/includes/image.php';
		}

		$gen_meta = \wp_generate_attachment_metadata( $attachment->post_id, $file->getRealPath() );

		return \wp_update_attachment_metadata( $attachment->post_id, $gen_meta );
	}

	/**
	 * Check if the $file has already been downloaded.
	 *
	 * @param string $filename The filename + extension.
	 * @return string|bool Return the file path or false.
	 */
	public function file_downloaded( $filename ) {
		$files = get_transient( 'rezi_uploaded_files' );

		if ( ! $files ) :
			$uploads_path = wp_upload_dir()['basedir'];
			$dir_iterator = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $uploads_path, \FilesystemIterator::SKIP_DOTS ) );

			$iter = new \CallbackFilterIterator(
				$dir_iterator,
				function( $cur, $key, $iter ) {
					$match           = false;
					$resized_pattern = sprintf( '/(\d+x\d+)\.%s$/', $cur->getExtension() );
					preg_match( $resized_pattern, $cur->getFilename(), $match );

					return ( ! $match );
				}
			);

			$files = [];
			foreach ( $iter as $file ) {
				$files[] = $file->getRealPath();
			}
			set_transient( 'rezi_uploaded_files', $files, HOUR_IN_SECONDS );
		endif;

		$same_file = array_filter(
			$files,
			function( $cur ) use ( $filename ) {
				$file_info = new \SplFileInfo( $cur );
				return $file_info->getFilename() == $filename;
			}
		);

		if ( count( $same_file ) > 0 ) {
			$file = array_pop( $same_file );

			error_log( json_encode( [ 'file_exists' => [ $filename, $file ] ] ) );

			return $file;
		}

		// File doesn't exists so clear cache as it will be uploaded.
		delete_transient( 'rezi_uploaded_files' );
		return false;
	}

	public function download_file( $url = '' ) {

		// If run by cron wp-admin is not loaded.
		if ( defined( 'DOING_CRON' ) || ! function_exists( 'download_url' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';
			require_once ABSPATH . 'wp-admin/includes/image.php';
		}

		$tmp = \download_url( $url );

		/**
		 * Check for download errors
		 * if there are error unlink the temp file name
		 */
		if ( is_wp_error( $tmp ) ) {
			@unlink( $tmp );
			error_log(
				json_encode(
					[
						'import_attachment' => 'download error',
						$tmp,
					]
				)
			);
			return $tmp;
		}
		$url_parsed = parse_url( $url );
		$url_info   = pathinfo( $url_parsed['path'] );
		$file       = $url_info['basename'];

		// Set up file similar to $_FILES upload array.
		$file_array = [
			'name'     => $file,
			'tmp_name' => $tmp,
		];
		$overrides  = [ 'test_form' => false ];

		$file = wp_handle_sideload( $file_array, $overrides );
		if ( isset( $file['error'] ) ) {
			error_log(
				json_encode(
					[
						'import_attachment' => 'upload error',
						$file,
					]
				)
			);
			@unlink( $tmp );
			return new \WP_Error( 'upload_error', $file['error'] );
		}
		@unlink( $tmp );

		return $file['file'];
	}
}
