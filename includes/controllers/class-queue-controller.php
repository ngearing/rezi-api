<?php

namespace Rezi\Controllers;

class Properties_Queue extends \Rezi\Modules\WP_Background_Process {

	protected $action = 'import_properties';

	protected function task( $item ) {

		if ( ! $item ) {
			return;
		}

		Properties_Controller::import_property( $item );

		return false;
	}

	protected function complete() {
		parent::complete();

		error_log( 'Property Batch Completed' );
	}
}

class Attachment_Queue extends \Rezi\Modules\WP_Background_Process {
	protected $action = 'import_attachments';

	protected function task( $item ) {

		if ( is_wp_error( $item ) ) {
			return false;
		}

		if ( ! isset( $item['post_id'] ) || ! isset( $item['url'] ) ) {
			return false;
		}

		error_log( json_encode( [ 'importing:', $item ] ) );

		Properties_Controller::import_attachment( $item );

		// Task complete remove item from queue.
		return false;
	}

	protected function complete() {
		parent::complete();
		error_log( 'Attachment Batch Completed' );
	}
}
