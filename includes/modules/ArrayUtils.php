<?php

namespace Rezi\Modules;

class ArrayUtils {

	function __construct( $array ) {
		$this->value = $array;

		return $this;
	}

	function get( $value, $key ) {

	}

	/**
	 * Turm multidimensional array in to associative array
	 * with array key concatinated with $sep.
	 *
	 * @param string $prefix
	 * @param string $sep
	 * @param array  $array
	 * @return array
	 */
	public function flatten( $prefix = '', $sep = '.', $array = [] ) {
		$result = array();

		foreach ( $array ?: $this->value as $key => $value ) {
			$new_key = $prefix . ( empty( $prefix ) ? '' : $sep ) . $key;

			if ( is_array( $value ) ) {
				$result = array_merge( $result, $this->flatten( $new_key, $sep, $value ) );
			} else {
				$result[ $new_key ] = $value;
			}
		}

		return $result;
	}
}
