<?php
/**
 * Api class.
 *
 * @package rezi-api
 */

namespace Rezi\Modules;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Api class for interating with an api.
 */
class Api {

	/**
	 * The API url or endpoint.
	 *
	 * @var string
	 */
	private $url;

	/**
	 * The API method, POST | GET
	 *
	 * @var string
	 */
	private $method;

	/**
	 * Params to be encoded in the url.
	 *
	 * @var array
	 */
	private $params;

	/**
	 * Header to be sent with request.
	 *
	 * @var array
	 */
	private $headers;

	/**
	 * Body of the request, will be json_encoded
	 *
	 * @var array
	 */
	private $body;

	public function __construct() {
		$this->url     = 'https://api.dezrez.com/api/simplepropertyrole/search';
		$this->method  = 'POST';
		$this->params  = [
			'APIKey' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2F1dGguZGV6cmV6LmNvbS9BcGlLZXlJc3N1ZXIiLCJhdWQiOiJodHRwczovL2FwaS5kZXpyZXouY29tL3NpbXBsZXdlYmdhdGV3YXkiLCJuYmYiOjE1MjU5NDEyNzksImV4cCI6NDY4MTYxNDg3OSwiSXNzdWVkVG9Hcm91cElkIjoiNDkwOTkxOSIsIkFnZW5jeUlkIjoiMjk4Iiwic2NvcGUiOlsiaW1wZXJzb25hdGVfd2ViX3VzZXIiLCJwcm9wZXJ0eV9iYXNpY19yZWFkIiwibGVhZF9zZW5kZXIiXX0.-8T-TE308dgLTlLNtl9eoX8GoA7pkhDwVr1Guf5XE-g',
		];
		$this->headers = [
			'Content-Type'     => 'application/json',
			'Rezi-Api-Version' => '1.0',
		];
		$this->body    = [
			'MarketingFlags' => [ 'ApprovedForMarketingWebsite' ],
			'IncludeStc'     => true,
			'PageSize'       => 100,
		];

		return $this;
	}

	public function get_request_data() {
		return [
			'method'  => $this->method,
			'headers' => $this->headers,
			'body'    => $this->body,
		];
	}

	public function set_request_data( $data = [] ) {
		$request = array_replace_recursive(
			[
				'method'  => $this->method,
				'headers' => $this->headers,
				'body'    => $this->body,
			],
			$data
		);

		$this->method  = $request['method'];
		$this->headers = $request['headers'];
		$this->body    = $request['body'];
	}

	public function request( $data = [] ) {

		if ( $data ) {
			$this->set_request_data( $data );
		}

		$request         = $this->get_request_data();
		$request['body'] = json_encode( $request['body'] );

		return wp_remote_request(
			$this->get_url(),
			$request
		);
	}

	private function get_url() {
		return add_query_arg( $this->params, $this->url );
	}
}
