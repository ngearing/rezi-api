<?php

namespace Rezi\Modules;

class Attachment {

	public $post_id;
	public $post;
	public $attached_file;
	public static $post_type = 'attachment';

	public function __construct( $post_id ) {
		$this->post_id = $post_id;
		$this->post    = get_post( $this->post_id );
		if ( ! $this->post ) {
			return false;
		}

		$this->attached_file = get_post_meta( $this->post_id, '_wp_attached_file', true );

		return $this;
	}

	/**
	 * Get attachment by file.
	 *
	 * @param string $file The file path or url to compare.
	 * @return Attachment|bool
	 */
	public static function get_by_file( $file = '' ) {
		$name  = sanitize_title( pathinfo( $file )['filename'] );
		$posts = get_posts(
			[
				'post_type'      => self::$post_type,
				'name'           => $name,
				'posts_per_page' => 1,
				'fields'         => 'ids',
			]
		);

		if ( $posts ) {
			return new self( array_pop( $posts ) );
		}

		return false;
	}

	public function get_file() {
		$attached_file = get_post_meta( $this->post_id, '_wp_attached_file', true );
		if ( ! $attached_file ) {
			return false;
		}

		$uploads = wp_upload_dir();
		return $uploads['basedir'] . '/' . $attached_file;
	}

	public function set_meta( array $post_meta ) {
		foreach ( $post_meta as $meta_key => $meta_val ) {
			update_post_meta( $this->post_id, $meta_key, $meta_val );

			if ( function_exists( 'get_field' ) ) {
				update_field( $meta_key, $meta_val, $this->post_id );
			}
		}
	}

}
