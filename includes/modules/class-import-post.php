<?php
/**
 * Import post class
 *
 * @package Rezi-Api
 */

namespace Rezi\Modules;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * This class handles the importing of a post
 */
class Import_Post {

	private $post_data;

	public $post_id;

	private $post_meta;

	private $post_terms;

	private $post_attachments;

	public function __construct( $post_data = [] ) {
		$this->filter_data( $post_data );

		// if ( $this->post_exists() ) {
		// $this->update_post();
		// } else {
		// $this->create_post();
		// }
		error_log(
			json_encode(
				[
					'importing' => $this->post_id,
				]
			)
		);

		// Update attachments first so we can assign their ids to meta fields later.
		// $this->update_attachments( $this->post_attachments );
		$this->update_terms();

		$this->update_meta();

		return $this;
	}

	/**
	 * Filters the input data against default data fields.
	 *
	 * @param array $data
	 * @return void
	 */
	private function filter_data( $data = [] ) {
		$defaults = [
			'ID'                    => 0,
			'post_author'           => 1,
			'post_date'             => '',
			'post_date_gmt'         => '',
			'post_content'          => '',
			'post_title'            => '',
			'post_excerpt'          => '',
			'post_status'           => 'publish',
			'comment_status'        => 'closed',
			'ping_status'           => 'closed',
			'post_password'         => '',
			'post_name'             => '',
			'to_ping'               => '',
			'pinged'                => '',
			'post_modified'         => '',
			'post_modified_gmt'     => '',
			'post_content_filtered' => '',
			'post_parent'           => 0,
			'guid'                  => '',
			'menu_order'            => 0,
			'post_type'             => 'post',
			'post_mime_type'        => '',
			'comment_count'         => 0,
			'post_meta'             => [],
			'post_terms'            => [],
			'post_attachments'      => [],
		];

		$post_data = [];
		foreach ( $defaults as $name => $default ) {
			if ( array_key_exists( $name, $data ) ) {
				$post_data[ $name ] = $data[ $name ];
			} else {
				$post_data[ $name ] = $default;
			}
		}

		$this->post_data        = $post_data;
		$this->post_id          = $post_data['ID'];
		$this->post_meta        = isset( $post_data['post_meta'] ) ? $post_data['post_meta'] : false;
		$this->post_terms       = isset( $post_data['post_terms'] ) ? $post_data['post_terms'] : false;
		$this->post_attachments = isset( $post_data['post_attachments'] ) ? $post_data['post_attachments'] : false;
	}

	public function set_post_id( int $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 * Check if a post exists
	 *
	 * @return bool
	 */
	private function post_exists() {

		global $wpdb;

		if ( isset( $this->post_data['post_meta']['estate_property_id'] ) ) {
			$post_id = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT post_id
					FROM $wpdb->postmeta
					WHERE meta_value = %s",
					$this->post_data['post_meta']['estate_property_id']
				)
			);

			if ( $post_id ) {
				$this->post_id = $post_id;
				return true;
			}
		}

		if ( ! isset( $this->post_data['post_meta']['estate_property_google_maps']['address'] ) ) {
			return;
		}

		$address = $this->post_data['post_meta']['estate_property_google_maps']['address'];

		// Could possibly return 2 results property-status = selling | letting.
		// So we need to check which term the post is in.
		$posts = $wpdb->get_col(
			$wpdb->prepare(
				"SELECT post_id
				FROM $wpdb->postmeta
				WHERE meta_value LIKE (%s)
				AND post_id IN (
					SELECT object_id
					FROM $wpdb->term_relationships
					WHERE term_taxonomy_id IN (
						SELECT term_id
						FROM $wpdb->terms
						WHERE slug = %s
					)
				)",
				'%' . str_replace( ' ', '%', $address ) . '%',
				str_replace( ' ', '-', strtolower( $this->post_data['post_terms']['property-status'] ) )
			)
		);

		if ( ! empty( $posts ) ) {
			$this->post_id = $posts[0]->post_id;
			return true;
		}

		return false;
	}

	/**
	 * Update post with new data
	 *
	 * @return void
	 */
	private function update_post() {
		$this->post_data['ID'] = $this->post_id;

		wp_update_post( $this->post_data );
	}

	/**
	 * Create a new post
	 *
	 * @return void
	 */
	private function create_post() {
		$post_data = $this->post_data;

		$this->post_id = wp_insert_post( $this->post_data );
	}

	/**
	 * Update terms for post
	 *
	 * @return void
	 */
	private function update_terms() {
		foreach ( $this->post_terms as $tax => $terms ) {

			if ( ! taxonomy_exists( $tax ) ) {
				continue;
			}

			if ( ! is_array( $terms ) ) {
				$terms = [ $terms ];
			}

			foreach ( $terms as $k => $term ) {
				$existing_term = term_exists( $term, $tax );

				if ( ! $existing_term ) {
					$term = wp_insert_term( $term, $tax, [ 'slug' => sanitize_title( $term ) ] );
					if ( ! is_wp_error( $term ) ) {
						$terms[ $k ] = (int) $term['term_id'];
					} else {
						error_log( $term->get_error_message() );
					}
				} elseif ( is_array( $existing_term ) ) {
					$terms[ $k ] = (int) $existing_term['term_id'];
				} else {
					$terms[ $k ] = (int) $existing_term;
				}
			}

			wp_set_object_terms( $this->post_id, $terms, $tax );
			$this->post_terms[ $tax ]         = $terms;
			$this->post_meta[ 'acf-' . $tax ] = $terms;
		}
	}

	/**
	 * Update meta for post
	 *
	 * @return void
	 */
	private function update_meta() {
		foreach ( $this->post_meta as $meta_key => $meta_val ) {
			update_post_meta( $this->post_id, $meta_key, $meta_val );

			if ( class_exists( 'ACF' ) ) {
				update_field( $meta_key, $meta_val, $this->post_id );
			}
		}
	}

	/**
	 * Update post attachments
	 *
	 * @return mixed
	 */
	private function update_attachments( $attachments, $return = false ) {
		if ( ! $attachments ) {
			return;
		}

		foreach ( $attachments as $field => $attachment ) {
			if ( ! is_array( $attachment ) ) {

				if ( ! $file_id = $this->image_exists( $attachment ) ) {
					// Download image to server.
					// And return a post id.
					$file_id = $this->download_file( $attachment );
				}

				if ( ! is_wp_error( $file_id ) ) {
					$attachments[ $field ] = (int) $file_id;
				}
			} else {
				$attachments[ $field ] = $this->update_attachments( $attachment, true );
			}

			if ( ! $return && is_string( $field ) ) {
				// Set attachment ids to their meta fields.
				$this->post_meta[ $field ] = $attachments[ $field ];
			}
		}

		// Return values if function used recursivly.
		if ( $return ) {
			return $attachments;
		}
	}

	/**
	 * Check if file exists
	 *
	 * @param string $url Url to original file.
	 * @return mixed
	 */
	private function image_exists( string $url ) {
		global $wpdb;

		$post_id = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT post_id
				FROM $wpdb->postmeta
				WHERE meta_value = %s",
				esc_url_raw( $url )
			)
		);

		if ( $post_id ) {
			return $post_id;
		}

		return false;
	}

	/**
	 * Download file and return id
	 *
	 * @param string $url Url to download file from.
	 * @return int
	 */
	private function download_file( $url ) {

		$org_url = false;
		// If file is an image.
		if ( in_array( pathinfo( $url, PATHINFO_EXTENSION ), [ 'jpg', 'png' ] ) ) {
			// Add params to get smaller image.
			$org_url = $url;
			$url     = add_query_arg(
				[
					'width'   => 2000,
					'quality' => 40,
				],
				$url
			);
		}

		// If run by cron wp-admin is not loaded.
		if ( defined( 'DOING_CRON' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';
			require_once ABSPATH . 'wp-admin/includes/image.php';
		}

		$tmp = \download_url( $url );

		$file_array = array(
			'name'     => basename( parse_url( $url, PHP_URL_PATH ) ),
			'tmp_name' => $tmp,
		);

		/**
		 * Check for download errors
		 * if there are error unlink the temp file name
		 */
		if ( is_wp_error( $tmp ) ) {
			@unlink( $file_array['tmp_name'] );
			return $tmp;
		}

		/**
		 * now we can actually use media_handle_sideload
		 * we pass it the file array of the file to handle
		 * and the post id of the post to attach it to
		 * $post_id can be set to '0' to not attach it to any particular post
		 */
		$post_id = $this->post_id;

		$id = media_handle_sideload( $file_array, $post_id );

		/**
		 * We don't want to pass something to $id
		 * if there were upload errors.
		 * So this checks for errors
		 */
		if ( is_wp_error( $id ) ) {
			@unlink( $file_array['tmp_name'] );
			return $id;
		}

		// Set meta value of src file.
		update_post_meta( $id, 'original_src', ( $org_url ?: $url ) );

		return $id;
	}
}
