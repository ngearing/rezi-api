<?php

namespace Rezi\Modules;

class Property {

	public $import_data;
	public $post_id;
	public $post;
	public $import_id;
	public static $post_type = 'property';

	public function __construct( $post_id ) {
		$this->post_id = $post_id;
		$this->post    = get_post( $this->post_id );
		if ( ! $this->post ) {
			return false;
		}

		return $this;
	}

	/**
	 * Get post by import data. Mainly import_id | estate_property_id.
	 *
	 * @param object $import_data The import json object.
	 * @return bool|object
	 */
	public static function get_by_import( \stdClass $import_data ) {
		if ( ! $import_data->RoleId ) {
			return false;
		}

		$posts = get_posts(
			[
				'fields'     => 'ids',
				'post_type'  => self::$post_type,
				'meta_key'   => 'estate_property_id',
				'meta_value' => $import_data->RoleId,
			]
		);

		if ( ! $posts ) {
			return false;
		}

		return new self( array_pop( $posts ) );
	}

	public static function create( array $post_data ) {
		$post_data['post_type']   = self::$post_type;
		$post_data['post_status'] = 'publish';
		$post_id                  = wp_insert_post( $post_data );

		if ( $post_id ) {
			return new self( $post_id );
		}

		return false;
	}

	public function update( array $post_data ) {
		return wp_update_post( $post_data );
	}

	public function get_import_data() {
		$data = get_post_meta( $this->post_id, 'import_data', true );
		if ( is_string( $data ) ) {
			$data = json_decode( $data );
		}
		return $data ?: false;
	}

	public function set_terms( array $post_terms ) {
		foreach ( $post_terms as $tax => $terms ) {

			if ( ! taxonomy_exists( $tax ) ) {
				continue;
			}

			if ( ! is_array( $terms ) ) {
				$terms = [ $terms ];
			}

			foreach ( $terms as $k => $term ) {
				$existing_term = term_exists( $term, $tax );

				if ( ! $existing_term ) {
					$terms[ $k ] = (int) wp_insert_term( $term, $tax, [ 'slug' => sanitize_title( $term ) ] )['term_id'];
				} elseif ( is_array( $existing_term ) ) {
					$terms[ $k ] = (int) $existing_term['term_id'];
				} else {
					$terms[ $k ] = (int) $existing_term;
				}
			}

			wp_set_object_terms( $this->post_id, $terms, $tax );
			// $this->post_terms[ $tax ]         = $terms;
			// $this->post_meta[ 'acf-' . $tax ] = $terms;
		}
	}

	public function set_meta( array $post_meta ) {
		foreach ( $post_meta as $meta_key => $meta_val ) {
			update_post_meta( $this->post_id, $meta_key, $meta_val );

			if ( function_exists( 'get_field' ) ) {
				update_field( $meta_key, $meta_val, $this->post_id );
			}
		}
	}
}
