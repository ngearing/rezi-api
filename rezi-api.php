<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Rezi_Api
 *
 * Plugin Name:       Rezi Api
 * Plugin URI:        https://bitbucket.org/ngearing/rezi-api/src
 * Description:       This plugin intergrates the Rezi Api into a WordPress theme.
 * Version:           1.0.1
 * Author:            GreenGraphics
 * Author URI:        https://nathangearing.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rezi-api
 * Bitbucket Plugin Uri: https://bitbucket.org/ngearing/rezi-api
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'REZI_API_VERSION', '1.0.1' );
define( 'REZI_API_DIR', plugin_dir_path( __FILE__ ) );
define( 'REZI_API_URL', plugin_dir_url( __FILE__ ) );
define( 'REZI_API_BASENAME', plugin_basename( __FILE__ ) );

require_once REZI_API_DIR . '/vendor/autoload.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-activator.php
 */
function activate_rezi_api() {
	// require_once REZI_API_DIR . 'includes/class-activator.php';
	\Rezi\Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-deactivator.php
 */
function deactivate_rezi_api() {
	// require_once REZI_API_DIR . 'includes/class-deactivator.php';
	\Rezi\Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_rezi_api' );
register_deactivation_hook( __FILE__, 'deactivate_rezi_api' );

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_rezi_api() {

	$plugin = new \Rezi\Rezi_Api();
	$plugin->run();

}
run_rezi_api();
